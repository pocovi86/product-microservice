package ar.com.microservice.product.service;

import ar.com.microservice.product.entity.Product;
import ar.com.microservice.product.entity.Category;

import java.util.List;

public interface ProductService {

    /**
     * devuelve una lista de productos
     * @return {@link List} of {@link Product}
     */
    List<Product> listAllProduct();

    /**
     * obtener un producto por medio de un id
     * @param id
     * @return {@link Product}
     */
    Product getProduct(Long id);

    /**
     * crea un producto
     * @param product
     */
    Product createProduct(Product product);

    /**
     * actualiza un producto
     * @param product
     */
    Product updateProduct(Product product);

    /**
     * elimina un producto por medio del id
     * @param id
     * @return
     */
    Product deleteProduct(Long id);

    /**
     * obtener una lista de producto por medio de una categoria
     * @param category
     * @return {@link List} of {@link Product}
     */
    List<Product> findByCategory(Category category);

    /**
     * actualizar el stock de un producto
     * @param id
     * @param quatity
     * @return {@link Product}
     */
    Product updateStock(Long id, Double quatity);
}
